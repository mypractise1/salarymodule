import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  constructor( private router: Router ) { }

  ngOnInit() {
  }

  /** 
   * @discription: doughnutChart labels and data
   * @author: surekha **/

  doughnutChartLabels: Label[] = ['Income', 'Expenses', 'Savings'];
  doughnutChartData: MultiDataSet = [
    [55, 25, 20]
  ];
  doughnutChartType: ChartType = 'doughnut';

  /** 
   * @description: add method for expensesform
   * @author: surekha **/

  addExpensesFunction(event) {
    this.router.navigate(['/expenseformpage'])
    .catch(console.error);
  }

  /** 
   * @description: add method for incomeForm
   * @author: surekha **/
  
  addIncomeFunction(event) {
    this.router.navigate(['/incomeformpage'])
    .catch(console.error);
  }

}
