import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpensesFormPageComponent } from './expenses-form-page.component';

describe('ExpensesFormPageComponent', () => {
  let component: ExpensesFormPageComponent;
  let fixture: ComponentFixture<ExpensesFormPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpensesFormPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpensesFormPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
